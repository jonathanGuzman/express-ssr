FROM node:10.4-slim

ENV NPM_CONFIG_LOGLEVEL warn

RUN mkdir -p /opt/backend

WORKDIR /opt/backend

COPY . /opt/backend

RUN chown -R node:node /opt/backend/*

RUN npm install --only=prod

EXPOSE 3000

USER node

CMD [ "node", "app/app.js" ]
